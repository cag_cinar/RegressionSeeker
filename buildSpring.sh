for filename in $(find . -name '*.gradle'); do
    if   [ -f "${filename}" ]
    then 
        sed -i "s/io.projectreactor.ipc:reactor-netty\"/io.projectreactor.ipc:reactor-netty:0.7.10.RELEASE\"/g" $filename
    fi
done

sed -i "s/io.projectreactor.ipc:reactor-netty/io.projectreactor.ipc:reactor-netty:0.7.10.RELEASE/g" ./spring-test/spring-test.gradle
./gradlew build -q -x test -x javadoc