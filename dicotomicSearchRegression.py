import subprocess
import os
import re
import json
import sys
import xml.etree.ElementTree as ET

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    GREY = '\u001b[38;5;245m'

class Out():
    INIT = 0
    SUCCESS = 1
    FAIL = 2
    ERROR = 3

def printc(c,s):
        print(c+s+bcolors.ENDC)

message_format = """
    ################################################################
    #   The author of """+bcolors.HEADER+"""%h"""+bcolors.ENDC+""" was """+bcolors.BOLD+"""%an"""+bcolors.ENDC+""", %ar
    #   The title was >>"""+bcolors.WARNING+"""%s"""+bcolors.ENDC+"""<<
    ################################################################
"""

class DicotomicSearchRegression():

    def __init__(self, config):

        self.id = config['id']
        self.project=config["project"]
        self.base_commit=config["base_commit"]
        self.build= config["build"]
        self.folder= config["folder"]
        self.file_=config["file"]
        self.source=self.folder+self.file_
        self.test_command= config["test_command"]
        self.test_report= config["test_report"]
        self.jump= config["jump"]
        self.limit= config["limit"]
        self.outfile = open('logs/%s.log' % config['id'], 'w+')
        self.n = 0
        self.status = Out.INIT
        self.last = self.base_commit
        self.success_commits = set([self.base_commit])
        self.error_commits = set()
        self.fails_commits = set()
        self.c_count = 0

    def call(self, params, output=None):
        if output is None:
            output=self.outfile
        return subprocess.call(params, shell=True, stdout=output, stderr=self.outfile)

    def runAndGetOutput(self, command):
        text=""
        with open('run', 'w') as out:
            self.call(command, output=out)
        with open('run', 'r') as out:
            text = out.read()
        self.call("rm run")
        return text


    def get_previous_commit(self,n=1):
        commit = self.runAndGetOutput("git show HEAD~%d | head -n 1 |  cut -d' ' -f2" % n)
        if commit is "":
            return None
        return commit.strip()

    def get_forward_commit(self,n=1):
        commit = self.runAndGetOutput("git log --reverse --pretty=%H master | grep -A "+str(n)+" $(git rev-parse HEAD) | tail -n1")
        if commit is "":
            return None
        return commit.strip()

    def nextCommit(self,n, out, commit_count):
        commit = None
        new_n = 0

        if out is Out.INIT:
            # GO BACK
            new_n = n
            commit = self.get_previous_commit(new_n)
            if commit is None:
                new_n = round(n/2)
                printc(bcolors.GREY,"> PROJECT COMMITS LIMIT REACHED: GO %d COMMITS BACK" % new_n)
                return self.nextCommit(new_n, out, commit_count)
            commit_count-=new_n
            printc(bcolors.GREY, "> GO %d COMMITS BACK (%d commits from fix commit)"%(new_n, commit_count))

        elif out is Out.SUCCESS:
            # GO FORWARD
            new_n = round(n/2)
            commit = self.get_forward_commit(new_n)
            if commit is None:
                new_n = round(new_n/2)
                printc(bcolors.GREY, "> PROJECT COMMITS LIMIT REACHED: GO %d COMMITS FORWARD"%new_n)
                return self.nextCommit(new_n, out, commit_count)
            commit_count+=new_n
            printc(bcolors.GREY, "> GO %d COMMITS FORWARD (%d commits from fix commit)"%(new_n, commit_count))
        
        elif out is Out.FAIL:
            # GO BACK
            if len(self.success_commits) > 1 or len(self.error_commits) > 0:
                new_n = n/2
            else:
                new_n = n*2
            commit = self.get_previous_commit(new_n)
            if commit is None:
                new_n = round(n/2)
                printc(bcolors.GREY, "> PROJECT COMMITS LIMIT REACHED: GO %d COMMITS BACK"%new_n)
                return self.nextCommit(new_n, out, commit_count)
            commit_count-=new_n
            printc(bcolors.GREY, "> GO %d COMMITS BACK (%d commits from fix commit)"%(new_n, commit_count))

        elif out is Out.ERROR:
            # GO FORWARD
            new_n = round(n/2)
            commit = self.get_forward_commit(new_n)
            if commit is None:
                new_n = round(new_n/2)
                printc(bcolors.GREY, "> PROJECT COMMITS LIMIT REACHED: GO %d COMMITS FORWARD"%new_n)
                return self.nextCommit(new_n, out, commit_count)
            commit_count+=new_n
            printc(bcolors.GREY, "> GO %d COMMITS FORWARD (%d commits from fix commit)"%(new_n, commit_count))

        return commit, new_n, commit_count


    def change_commit(self,commit_hash):
        self.call("git checkout -f .")
        self.call("git checkout -f %s" % commit_hash)

    def printAndSaveReport(self,commit):
        with open('../%s_report' % self.id, 'w+') as out:
            self.call("git show %s" % commit, output=out)

        with open('aux', 'w+') as out:
            self.call("git show --format='%s' -s %s" % (message_format, commit), output=out)
        with open('aux', 'r') as out:
            print(out.read())
        self.call("rm aux")

    def clean(self):
        # RESTORE GIT HEAD
        self.call("rm %s" % self.source)
        self.call("rm ../testFile")
        self.change_commit(self.base_commit)
        self.outfile.close()

    def getFailures(self,report_path):
        tree = ET.parse(report_path)
        testsuite= tree.getroot()
        hasFails = False
        fails = []
        for case in testsuite.iter('testcase'):
            if case.find('failure') != None or case.find('error') != None:
                if case.find('failure') != None:
                    fails.append("FAILURE: "+case.find('failure').attrib['message'])
                if case.find('error') != None:
                    fails.append("ERROR: "+case.find('error').attrib['message'])
                hasFails=True
        return (hasFails, fails)

    def search(self):
        # MOVE TO PROJECT DIRECTORY
        os.chdir(os.getcwd()+"/"+self.project)
        self.change_commit(self.base_commit)

        # GET TEST FILE OUT PROJECT
        if not os.path.isfile(self.source):
            raise Exception("TEST FILE DOESN'T EXIST")
        self.call("cp %s %s"%(self.source, "../testFile"))

        # CHECK TEST PASS
        self.call(self.build)
        returncode = self.call(self.test_command)
        if returncode != 0:
            raise Exception("NOT A SUCCESS TEST")
        print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test (FIXED COMMIT)"%self.base_commit[0:10])

        # CHECK IF TEST PASS FOR PREVIOUS COMMITS

        while not self.jump == 0 and self.n<self.limit:
            c, self.jump, self.c_count = self.nextCommit(self.jump, self.status, self.c_count)
            self.call("rm -rf %s" % self.folder)
            self.change_commit(c)
            self.call("mkdir -p %s" % self.folder)
            self.call("cp %s %s"%("../testFile", self.source))
            self.call(self.build)
            # REMOVE TEST REPORT
            self.call("rm %s" % self.test_report)
            # RUN TEST COMMAND TO GENERATE A NEW REPORT
            self.call(self.test_command)

            if not os.path.isfile(self.test_report):
                # ERROR
                print("\033[95m%s\033[0m commit \033[91mhad an error\033[0m when test was running, no test report found"%c[0:10])
                self.status = Out.ERROR
                self.error_commits.add(c)
                next_ = self.get_forward_commit()
                if next_ in self.fails_commits:
                    print("\033[95m%s\033[0m commit contains an error, \033[95m%s\033[0m fail at test, no regression detected"%(c[0:10], next_[0:10]))
                    break
            else:
                hasFails, fails = self.getFailures(self.test_report)
                if hasFails:
                    # FAIL
                    print("\033[95m%s\033[0m commit \033[91mdoesn't pass\033[0m the test"%c[0:10])
                    for fail in fails[:-1]:
                        print(bcolors.GREY+" ┣ "+fail+bcolors.ENDC)
                    print(bcolors.GREY+" ┗ "+fails[-1]+bcolors.ENDC)

                    prev = self.get_previous_commit()

                    if prev in self.success_commits:
                        print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test, \033[95m%s\033[0m commit contains an \033[91merror (REGRESSION)\033[0m"%(prev[0:10], c[0:10]))
                        self.printAndSaveReport(c)
                        break
                    if prev in self.error_commits:
                        print("%s commit was an error, %s was a fail, no regression detected"%(prev[0:10], c[0:10]))
                        break
                    self.status = Out.FAIL
                    self.fails_commits.add(c)
                else:
                    # SUCCESS
                    if self.last == self.get_forward_commit():
                        print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test, \033[95m%s\033[0m commit contains an \033[91merror (REGRESSION)\033[0m"%(c[0:10], self.last[0:10]))
                        self.printAndSaveReport(self.last)
                        break
                    else:
                        if c == self.base_commit:
                            print("BASE REACHED")
                            self.status = Out.INIT
                        else:
                            print("\033[95m%s\033[0m commit \033[92mpass\033[0m the test" % c[0:10] )
                            self.status = Out.SUCCESS
                            self.success_commits.add(c)
                self.last = c
            self.n+=1
        self.clean()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Use: python script.py <config_file>")
        exit()
    config = json.load(open(sys.argv[1]))
    dsr = DicotomicSearchRegression(config)
    dsr.search()
