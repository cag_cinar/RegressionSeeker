from dicotomicSearchRegression import DicotomicSearchRegression
import json
import os

currentDir = os.getcwd()

for name in os.listdir("configFiles/"):
    if(name == "base.json"): continue
    if(name == "BASIC-0.json"): continue
    print("RUNNING SEARCH FOR: "+name+"\n")
    try:
        config = json.load(open("configFiles/"+name))
        dsr = DicotomicSearchRegression(config)
        dsr.search()
    except Exception as ex:
        print("ERROR AT SEATCH AT "+name)
        print(ex)
    os.chdir(currentDir)

